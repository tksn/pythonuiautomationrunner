from __future__ import print_function
import sys
import logging
import os
import time
try:
    import uiautomatorclient as uiautomator
    RUNNING_ON = 'remote'
except ImportError:
    import uiautomator
    RUNNING_ON = 'local'


def main():
    print('Running...')

    dev = uiautomator.Device()

    # Print device info (to logcat/main)
    print('device_info={}'.format(dev.info))

    # press keys
    dev.press.home()
    time.sleep(1)
    dev.press.recent()
    time.sleep(1)
    dev.press.back()
    time.sleep(2)

    # open notification/quick settings
    dev.open.notification()
    time.sleep(5)
    dev.press.home()
    time.sleep(3)    
    dev.open.quick_settings()
    time.sleep(5)
    dev.press.home()
    time.sleep(3)

    # click
    dev(text='Google').click()
    dev.wait.update()
    dev(text='Gmail').click.wait()
    time.sleep(2)
    dev.press.home()

    # long click / scrool
    workspace = dev(resourceId='com.google.android.googlequicksearchbox:id/workspace')
    workspace.child(longClickable=True).long_click()
    time.sleep(3)
    dev.press.home()
    time.sleep(2)


def ensure_uiautomator_server_inactive(progress):
    import tksn.adb

    dev = tksn.adb.Device()
    progress('  Stopping residual uiautomator server in case actually it is still alive')
    uiautomator.device.server.stop()
    progress('  Restarting adb server')
    progress('    Please tap "OK" on "Allow usb debugging" popup on the device')
    dev.kill_server()
    dev.start_server()
    dev.wait_for_device()
    progress('  Uninstalling residual uiautomator app')
    dev.uninstall('com.github.uiautomator')


if __name__ == '__main__':
    if RUNNING_ON == 'local':
        logging.basicConfig(level=logging.DEBUG)
        ensure_uiautomator_server_inactive(print)
        uiautomator.device.server.start()
    main()

