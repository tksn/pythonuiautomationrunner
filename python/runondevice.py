from __future__ import print_function
import argparse
import sys
import os
import uiautomator
import tksn.adb
from tksn.adb.activitymanager import Intent


PACKAGE_NAME = 'org.bitbucket.tksn.pythonuiautomatorrunner'
CLASS_NAME = 'UIAutomatorRunnerService'
COMPONENT_NEME = PACKAGE_NAME + '/.' + CLASS_NAME
SCRIPT_FILEPATH_ON_DEVICE = '/sdcard/uiautomator_script.py'
ACTION_RUN = PACKAGE_NAME + '.action.RUN'
PARAMNAME_FILE = PACKAGE_NAME + '.extra.PARAM_FILE'
ACTION_CHECK_PERMISSION = PACKAGE_NAME + '.action.CHECK_PERMISSION'
RUNNER_APK = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'app-release.apk')
RUNNER_PERMISSIONS = ('android.permission.READ_EXTERNAL_STORAGE',)


def get_runner_apk():
    return RUNNER_APK


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('file', help='file to be executed on the device')
    args = parser.parse_args()
    return args


def print_stderr(s, end='\n'):
    print(s, file=sys.stderr, end=end)


def ensure_uiautomator_server_inactive(dev, progress):
    """Ensure uiautomator server is inactive

    Since residual uiautomator process in the device can cause some issue,
    it is better to remove it first. In order to do that,
    this function stops uiautomator server and then
    uninstalls uiautomator app from the device.
    """
    progress('  Stopping residual uiautomator server in case actually it is still alive')
    uiautomator.device.server.stop()
    progress('  Restarting adb server')
    progress('    Please tap "OK" on "Allow usb debugging" popup on the device')
    dev.kill_server()
    dev.start_server()
    dev.wait_for_device()
    progress('  Uninstalling residual uiautomator app')
    dev.uninstall('com.github.uiautomator')


def start_uiautomator_server(dev):
    print_stderr('Checking if uiautomator server is alive... ', end='')

    os.environ['JSONRPC_TIMEOUT'] = '5'
    if not uiautomator.device.server.alive:
        print_stderr('no')
        ensure_uiautomator_server_inactive(dev, print_stderr)
    else:
        print_stderr('yes')
    # WORKAROUND:
    # Restarting server even when it is alive.
    # It should not be necessary, but actually it is. I haven't looked into this issue.
    print_stderr('Restarting uiautomator server... ', end='')
    uiautomator.device.server.start()
    print_stderr('done')


def ensure_runner_installed(dev):
    print_stderr('Checking if Python UIAutomator Runner service is installed... ', end='')

    packages = dev.pm.list_packages()
    if not any(PACKAGE_NAME in package for package in packages):
        print_stderr('no')
        print_stderr('  Installing Python UIAutomator Runner... ', end='')
        dev.install(get_runner_apk())
        print_stderr('done')
    else:
        print_stderr('yes')

    print_stderr('Adding STORAGE permission to Python UIAutomator Runner... ', end='')
    for permission in RUNNER_PERMISSIONS:
        dev.pm.grant(PACKAGE_NAME, permission)
    dev.am.startservice(
        Intent(action=ACTION_CHECK_PERMISSION, component=COMPONENT_NEME))
    print_stderr('done')


def run(dev, filename):
    if not os.path.exists(filename):
        filename = os.path.abspath(filename)
        if not os.path.exists(filename):
            raise ValueError('File: {} not found.'.format(filename))
    print_stderr('Pushing script file to device: {} -> {} ...'.format(
        filename, SCRIPT_FILEPATH_ON_DEVICE), end='')
    dev.push(filename, SCRIPT_FILEPATH_ON_DEVICE)
    print_stderr('done')
    print_stderr('Starting running script...', end='')
    dev.am.startservice(
        Intent(
            action=ACTION_RUN, component=COMPONENT_NEME,
            extras={PARAMNAME_FILE: SCRIPT_FILEPATH_ON_DEVICE}))
    print_stderr('done')


def main():
    args = parse_args()
    dev = tksn.adb.Device()

    start_uiautomator_server(dev)
    ensure_runner_installed(dev)
    run(dev, args.file)


if __name__ == '__main__':
    main()

