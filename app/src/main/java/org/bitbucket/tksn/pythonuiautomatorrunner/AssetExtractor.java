package org.bitbucket.tksn.pythonuiautomatorrunner;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

/**
 * Created by tksn on 2016/07/05.
 * Bollowed from http://joaoventura.net/blog/2014/python-android-4/
 */
public class AssetExtractor {

    private final static String VERSION_FILE = "dataversion.txt";

    private Context mContext;
    private AssetManager mAssetManager;
    private Iterable<String> mCopyDirs;

    public AssetExtractor(Context context, Iterable<String> copyDirs) {
        mContext = context;
        mAssetManager = context.getAssets();
        mCopyDirs = copyDirs;
    }

    public String getDataFilesPath() {
        return mContext.getApplicationInfo().dataDir + "/files/";
    }

    private boolean isDataLatest() {
        try {
            byte[] bufAsset = new byte[1024];
            int readAsset = mAssetManager.open(VERSION_FILE).read(bufAsset);
            byte[] bufData = new byte[1024];
            int readData = (new FileInputStream(
                    new File(getDataFilesPath(), VERSION_FILE))).read(bufData);
            return Arrays.equals(
                    Arrays.copyOf(bufAsset, readAsset), Arrays.copyOf(bufData, readData));
        }
        catch (IOException e) {
            return false;
        }
    }

    public void run() {
        if (!isDataLatest()) {
            cleanDataFiles();

            for (String dir : mCopyDirs) {
                copyAssetRecursive(dir);
            }
            copyFileAsset(VERSION_FILE);
        }
    }

    private void cleanDataFiles() {
        File rootDir = new File(this.getDataFilesPath());
        this.recursiveDelete(rootDir);
        rootDir.mkdir();
    }

    private void recursiveDelete(File file) {
        if (file.isDirectory()) {
            for (File f : file.listFiles())
                recursiveDelete(f);
        }
        file.delete();
    }

    private void copyAssetRecursive(String path) {
        try {
            String[] assetList = mAssetManager.list(path);
            if (assetList == null || assetList.length == 0)
                throw new IOException();

            // Make the directory.
            File dir = new File(this.getDataFilesPath(), path);
            dir.mkdirs();

            // Recurse on the contents.
            for (String entry : assetList) {
                copyAssetRecursive(path + "/" + entry);
            }
        } catch (IOException e) {
            copyFileAsset(path);
        }
    }

    private void copyFileAsset(String path) {
        File file = new File(getDataFilesPath(), path);
        try {
            InputStream in = mAssetManager.open(path);
            OutputStream out = new FileOutputStream(file);
            byte[] buffer = new byte[1024];
            int read = in.read(buffer);
            while (read != -1) {
                out.write(buffer, 0, read);
                read = in.read(buffer);
            }
            out.close();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
