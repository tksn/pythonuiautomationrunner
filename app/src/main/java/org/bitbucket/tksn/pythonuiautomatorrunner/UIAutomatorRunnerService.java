package org.bitbucket.tksn.pythonuiautomatorrunner;

import android.Manifest;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import java.io.FileNotFoundException;
import java.util.Arrays;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class UIAutomatorRunnerService extends IntentService {
    private static final String ACTION_RUN =
            "org.bitbucket.tksn.pythonuiautomatorrunner.action.RUN";
    private static final String ACTION_CHECK_PERMISSION =
            "org.bitbucket.tksn.pythonuiautomatorrunner.action.CHECK_PERMISSION";
    private static final String EXTRA_PARAM_FILE =
            "org.bitbucket.tksn.pythonuiautomatorrunner.extra.PARAM_FILE";
    private static final int NOTIFICATION_ID_START = 1;
    private static final int NOTIFICATION_ID_STOP = 2;
    private static final int NOTIFICATION_ID_PERMISSION = 3;

    public UIAutomatorRunnerService() {
        super("UIAutomatorRunnerService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_RUN.equals(action)) {
                final String file = intent.getStringExtra(EXTRA_PARAM_FILE);
                handleActionRun(file);
            }
            else if (ACTION_CHECK_PERMISSION.equals(action)) {
                handleActionCheckPermission();
            }
        }
    }

    /**
     * Handle action Check permission in the provided background thread with the provided
     * parameters.
     */
    private void handleActionCheckPermission() {
        int checkResult = ContextCompat.checkSelfPermission(
                this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (checkResult != PackageManager.PERMISSION_GRANTED) {
            showNotification(
                    NOTIFICATION_ID_PERMISSION,
                    "Action required",
                    "Grant Storage permission to Python UIAutomator Runner",
                    "It can be done in Settings > Apps > Python UIAutomator Runner > Permissions"
            );
        }
    }

    /**
     * Handle action Run in the provided background thread with the provided
     * parameters.
     */
    private void handleActionRun(String file) {
        final String[] extractDirs = {"python"};
        AssetExtractor ax = new AssetExtractor(this, Arrays.asList(extractDirs));
        ax.run();
        String pythonPath = ax.getDataFilesPath() + "python";

        PythonInterpreter.start(pythonPath);
        showStartNotification(file);
        try {
            int runResult = PythonInterpreter.runFile(file);
            if (runResult == PythonInterpreter.Result.FILE_ERROR)
                throw new FileNotFoundException(file + " not found");
            if (runResult == PythonInterpreter.Result.SCRIPT_ERROR)
                throw new RuntimeException("exception occurred");
        }
        catch (Exception e) {
            showStopNotification(file, "Error(" + e.getMessage() + ")");
            return;
        }
        finally {
            PythonInterpreter.end();
        }
        showStopNotification(file, "Completed");
    }

    private void showStartNotification(String filePath) {
        showNotification(
                NOTIFICATION_ID_START,
                "UIAutomator started",
                "Script file:" + filePath,
                ""
        );
    }

    private void showStopNotification(String filePath, String statusString) {
        showNotification(
                NOTIFICATION_ID_STOP,
                "UIAutomator stopped",
                "Reason: " + statusString,
                "Script file: " + filePath
        );
    }

    private void showNotification(int id, String title, String text, String subText) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle(title);
        builder.setContentText(text);
        builder.setSubText(subText);
        builder.setAutoCancel(true);
        builder.setPriority(Notification.PRIORITY_HIGH);
        NotificationManager manager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify(id, builder.build());
    }
}
