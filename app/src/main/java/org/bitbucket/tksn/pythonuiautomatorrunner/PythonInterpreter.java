package org.bitbucket.tksn.pythonuiautomatorrunner;

/**
 * Created by tksn on 2016/07/06.
 */
public class PythonInterpreter {

    public static class Result {
        public final static int OK = 0;
        public final static int FILE_ERROR = -1;
        public final static int SCRIPT_ERROR = -2;
    }

    public static native void start(String pythonPath);
    public static native void end();
    public static native int runFile(String filePath);

    static {
        System.loadLibrary("python2.7");
        System.loadLibrary("pythoninterpreter");
    }
}
