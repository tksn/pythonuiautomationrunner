#include <jni.h>
#include <Python.h>
#include <android/log.h>

#define APPNAME "PythonUIAutomatorRunner"
#define LOG(x) __android_log_write(ANDROID_LOG_INFO, APPNAME, (x))

enum {
    RESULT_OK = JNI_OK,
    RESULT_FILE_ERROR = -1,
    RESULT_SCRIPT_ERROR = -2
};

static PyObject *androidembed_log(PyObject *self, PyObject *args) {
    char *logstr = NULL;
    if (!PyArg_ParseTuple(args, "s", &logstr)) {
        return NULL;
    }
    LOG(logstr);
    Py_RETURN_NONE;
}

static PyMethodDef AndroidEmbedMethods[] = {
        {"log", androidembed_log, METH_VARARGS,
                "Log on android platform"},
        {NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC init_androidembed(void) {
    (void) Py_InitModule("androidembed", AndroidEmbedMethods);
    /* inject our bootstrap code to redirect python stdin/stdout */
    PyRun_SimpleString(
            "import sys\n" \
            "import androidembed\n" \
            "class LogFile(object):\n" \
            "    def __init__(self):\n" \
            "        self.buffer = ''\n" \
            "    def write(self, s):\n" \
            "        s = self.buffer + s\n" \
            "        lines = s.split(\"\\n\")\n" \
            "        for l in lines[:-1]:\n" \
            "            androidembed.log(l)\n" \
            "        self.buffer = lines[-1]\n" \
            "    def flush(self):\n" \
            "        return\n" \
            "sys.stdout = sys.stderr = LogFile()\n"
    );
}

JNIEXPORT void JNICALL
Java_org_bitbucket_tksn_pythonuiautomatorrunner_PythonInterpreter_start(JNIEnv *env, jclass type,
                                                                        jstring pythonPath_) {
    const char *pythonPath = (*env)->GetStringUTFChars(env, pythonPath_, 0);

    setenv("PYTHONHOME", pythonPath, 1);
    setenv("PYTHONPATH", pythonPath, 1);

    Py_OptimizeFlag = 1;
    Py_Initialize();
    init_androidembed();
    PyRun_SimpleString(
            "import sys\n" \
            "import os\n" \
            "sys.path.append(os.path.join(sys.prefix, 'lib/site-python'))");

    (*env)->ReleaseStringUTFChars(env, pythonPath_, pythonPath);
}

JNIEXPORT void JNICALL
Java_org_bitbucket_tksn_pythonuiautomatorrunner_PythonInterpreter_end(JNIEnv *env, jclass type) {
    Py_Finalize();
}

JNIEXPORT jint JNICALL
Java_org_bitbucket_tksn_pythonuiautomatorrunner_PythonInterpreter_runFile(JNIEnv *env, jclass type,
                                                                          jstring filePath_) {
    const char *filePath = (*env)->GetStringUTFChars(env, filePath_, 0);

    int retval = RESULT_OK;
    LOG(filePath);
    FILE *fp = fopen(filePath, "r");
    if (fp == NULL)
        retval = RESULT_FILE_ERROR;
    else if (PyRun_SimpleFileEx(fp, filePath, 1) != 0)
        retval = RESULT_SCRIPT_ERROR;

    (*env)->ReleaseStringUTFChars(env, filePath_, filePath);
    return retval;
}
