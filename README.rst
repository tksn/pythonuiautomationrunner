Python UIAutomator Runner
=========================

Purpose
-------

This software enables to run a python script which utilizes uiautomator_ on Android.

.. _uiautomator: https://github.com/xiaocong/uiautomator


Status
------

Pre-alpha or feasibility study level.


Notes
-----

This repository includes,

- Modified version of uiautomator_
- Python (2.7.2) headers and dynamic-link library

See LICENSE.txt for license terms of each softwares.

